export default class Serializable {
	toString() {
		var cache = [];
		var ret = JSON.stringify(this, (k,v) => {
		    return (typeof v !== 'object' && v !== null) ? ((cache.indexOf(v) !== -1) ? void 0 : cache.push(v), v) : v;
		});
		cache = null; // Enable garbage collection
		return ret;
	}
}
