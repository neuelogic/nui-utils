export default class Jaxy {
	constructor() {
		this.xhr = this.newXmlHttpRequest();
	}

	newXmlHttpRequest() {
		if(typeof global !== "undefined") {
			try {
				return new global.XMLHttpRequest();
			} catch(e) {
				return false;
			}
		}

		try {
			return new XMLHttpRequest();
		} catch (e) {
			try {
				return new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					return new ActiveXObject("Microsoft.XMLHTTP");
				} catch (failed) {
					return false;
				}
			}
		}
	}

	trimString(str) {
		return (String.prototype.trim) ? str.trim() : str.replace(/^\s+/,'').replace(/\s+$/,'');
	}

	resolveRelativePath(path) {
		try {
			window = window || global.window;
		} catch(e) {}

		if(path.substr(0,1)==="/") {
			return window.location.origin + path;
		} else if(!/^[A-Za-z]+\:\/\//.test(path)) {
			return (
				window.location.origin +
				window.location.pathname + "/" +
				path
			);
		}

		return path;
	}

	createPostParamString(params){
		let ret = "";

		for(var x in params) {
			ret = this.appendParameterToString(ret, x, params[x]);
		}

		return ret;
	}

	appendParamArrToUrl(url, params) {
		for(var x in params) {
			url = this.appendParameterToUrl(url, x, params[x]);
		}
		return url;
	}
	appendParameterToString(str, param, value, delim){
		if(typeof str === "string" && typeof delim !== "string") {
			delim = "&";
		}

		str += delim+encodeURIComponent(param)+"="+encodeURIComponent(value);

		return str;
	}
	appendParameterToUrl(url, param, value) {
		var qPos = url.indexOf('?'),
			lChar = url.substr(-1, 1),
			delim = "";

		if (qPos > -1 && lChar != "&" && lChar != "?") {
			delim = "&";
		} else {
			delim = "?";
		}

		return this.appendParameterToString(url, param, value, delim);
	}
	injectMethodIntoArguments(method, args){
		if(typeof args[0] !== "object") args[0] = {};
		args[0].method = method;
		return args;
	}
	call(options, url, success, failure){
		var ajax = this.newXmlHttpRequest();
		if (!ajax) return false; /* @todo ERROR LOGGING */

		if(typeof url === "undefined" || typeof url === "function") {
			failure = success;
			success = url;
			url = options.url;
		}

		var async = true,//options.async,
			postBody = options.postBody,
			params = options.params,
			method = options.method.toUpperCase() || "GET",
			paramStr = "",
			contentType = options.contentType;

		if(params && typeof params === "object") {
			contentType = contentType || "application/x-www-form-urlencoded";

			if(method==="POST") {
				paramStr = this.createPostParamString(params);
			} else {
				url = this.appendParamArrToUrl(url,params);
			}
		} else if(method==="POST" && postBody) {
			contentType = contentType || "application/json";

			paramStr = postBody;
		}

		url = this.resolveRelativePath(
			this.appendParameterToUrl(url,"_",Math.random())
		);

		if (async) {
			ajax.onreadystatechange = () => {
				this.handleResponse(ajax,options,success,failure);
			}
		}

		options.requestedUrl = url;
		options.requestedMethod = method;

		ajax.open(method, url, async);
		ajax.setRequestHeader("Content-type", contentType);

		if(typeof options.headers === "object") {
			for(var x in options.headers){
				if(!options.headers[x]) continue;
				ajax.setRequestHeader(x, options.headers[x]);
			}
		}

		if(method==="POST") {
			options.paramStr = paramStr;
			ajax.send(paramStr);
		} else {
			ajax.send();
		}

		if (!async) {
			return this.handleResponse(ajax,options,success,failure);
		}

		return false;
	}

	get() {
		return this["call"].apply(this,this.injectMethodIntoArguments("GET",arguments));
	}

	post() {
		return this["call"].apply(this,this.injectMethodIntoArguments("POST",arguments));
	}

	handleResponse(request, options, success, failure) {
		if(request.readyState !== 4) return; // not ready yet!

		if(request.status < 100) return; // not a valid HTTP Status Code; assume cancelled

		if(request.status !== 200) { // non-200 HTTP Code means non-success
			/* @todo ERROR LOGGING */
			// throw new Error("Error retrieving modal template. Response: "+request.status);
			if (typeof failure !== "function") return;
			return failure(request.responseText, this, options);
		}

		if(typeof success !== "function") return; // bad success function, don't do anything

		if(options.responseType==="json") {
			var jsonStr;

			try {
				var jsonStr = JSON.parse(request.responseText);
			} catch(e) {
				/* @todo ERROR LOGGING */
				return failure(request.responseText, this, options);
			}

			//if(typeof jsonStr === "object") {
				return success(jsonStr, this, options);
			/*} else {
				return failure(request.responseText, this, options);
			}*/
		} else {
			return success(request.responseText, this, options);
		}
	}
}
