import util from 'util';

const LOG_QUIET = 0;
const LOG_LOUD = 1;
const LOG_VERBOSE = 2;

export default class NeueLogger {
	constructor(prefix, verbosity) {
		this.prefix = `${prefix}`;

		setConsts(this);
		this.verbosity = this.normalizeVerbosity(verbosity, true);
		this.print(`[LOGGER] Verbosity set to "${
			this.verbosity === LOG_QUIET ? 'quiet' :
			this.verbosity === LOG_LOUD ? 'loud' :
			this.verbosity === LOG_VERBOSE ? 'verbose' : 'quiet'
		}"`);
	}

	error(e) {
		if(e instanceof Error) {
			this.print(`[ERROR] ${e.stack}`);
		} else {
			this.print("[ERROR] "+e);
		}
		if(BUILD_ARGS.hasOwnProperty('dump')) {
			this.print(util.inspect(this.toString(), false, null));
		}
		setTimeout( () => process.exit(1), 500 );
	}

	print(msg, verbosity){
		verbosity = this.normalizeVerbosity(verbosity);
		if(verbosity > this.verbosity) return;
		const d = new Date();
		console.log(`[${this.getTimestamp()}] [${this.prefix}] ${msg}`);
	}

	normalizeVerbosity(v,log) {
		if([
			LOG_QUIET, LOG_LOUD, LOG_VERBOSE
		].indexOf(v)===-1) {
			return LOG_QUIET;
		}
	}

	getTimestamp() {
		const d = new Date();
		const mins = d.getMinutes();
		const secs = d.getSeconds();
		return `${
			d.getHours()
		}:${
			mins < 10 ? `0${mins}` : mins
		}:${
			secs < 10 ? `0${secs}` : secs
		}`;
	}

	attach(inst, prefix) {
		Object.defineProperties(inst, {
			log: {
				enumerable: false,
				writable: false,
				configurable: false,
				value: (msg,verbosity) => {
					if(prefix) return this.print(`[${prefix}] ${msg}`,verbosity);
					return this.print(msg,verbosity);
				}
			}
		});
		setConsts(inst);
	}
}

setConsts(NeueLogger);

function setConsts(obj){
	Object.defineProperties(obj, {
		LOG_QUIET: {
			enumerable: false,
			writable: false,
			configurable: false,
			value: LOG_QUIET
		},
		LOG_LOUD: {
			enumerable: false,
			writable: false,
			configurable: false,
			value: LOG_LOUD
		},
		LOG_VERBOSE: {
			enumerable: false,
			writable: false,
			configurable: false,
			value: LOG_VERBOSE
		}
	});
}
