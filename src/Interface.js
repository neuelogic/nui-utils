class NotImplemented extends Error {
	constructor(msg,f,ln){ super("The method or operation is not implemented: "+msg+"()",f,ln); }
}

export default class Interface {
	constructor(){ }
	NotImplemented(name){ throw new NotImplemented(this.constructor.name+"."+name); }
}
