import SingletonPromise from './SingletonPromise.js';

export default class Dispatcher {
	constructor(promiseEngine, Logger) {
		if(Logger && Logger.attach) Logger.attach(this, 'dispatcher');

		this.promiseEngine = promiseEngine || SingletonPromise;

		this.__promises = {};
		this.__resolvers = {};
	}

	when(eventName) {
		this.log(`when(${eventName})`, this.LOG_LOUD);
		return this.getPromise(eventName);
	}

	resolve(eventName, val) {
		this.log(`resolve(${eventName})`, this.LOG_LOUD);
		this.getResolver(eventName).resolve(val);
	}

	reject(eventName, val) {
		this.log(`reject(${eventName})`, this.LOG_LOUD);
		this.getResolver(eventName).reject(val);
	}

	unresolve(eventName) {
		this.log(`unresolve(${eventName})`, this.LOG_LOUD);
		let ur = this.getResolver(eventName).unresolve;
		if(ur && typeof ur === "function") ur();
	}

	getPromise(eventName, ifNotCreate = true) {
		//this.log(`getPromise(${eventName}, ${ifNotCreate})`, this.LOG_DEBUG);
		if(this.__promises.hasOwnProperty(eventName)) return this.__promises[eventName];
		else if(ifNotCreate===true) return this.createPromiseFor(eventName);
		else return false;
	}

	getResolver(eventName) {
		//this.log(`getResolver(${eventName})`, this.LOG_DEBUG);
		if(this.__resolvers.hasOwnProperty(eventName)) return this.__resolvers[eventName];
		else return this.createPromiseFor(eventName), this.__resolvers[eventName];
	}

	createPromiseFor(eventName) {
		//this.log(`createPromiseFor(${eventName}) **`, this.LOG_DEBUG);
		return this.__promises[eventName] = new (this.promiseEngine)( (resolve,reject,unresolve) => {
			this.__resolvers[eventName] = {
				"resolve": resolve,
				"reject": reject,
				"unresolve": unresolve
			};
		}, (e) => this.log(['ERROR',e.stack]));
	}

	log() { return; }
}
