/**
 * NEW REF SYNTAX: type$moduleName:resPath[/resPath[/resPath]]
 **/

/** TODO: UNIFORM GUID's FOR RESOURCES?? HOW DO WE MAKE REFS CONSISTENT!? **/

const ERRORS = {};

class ResourceRef {
	constructor(origPath, serialized) {
		if(this.isResRef(origPath)) {
			this.type = origPath.type;
			this.module = origPath.module;
			this.path = origPath.path;
			return;
		}

		if(!origPath) return;

		if(serialized) this.unserialize(origPath);
		else {
			this._ref = origPath;

			try {
				this.parse();
			} catch(e) {
				// fail silently the first time,
				// in case they want to build it
				// and then manually parse...
			}
		}
	}

	isResRef(inst = this) {
		if (typeof inst.get !== "function") return false;

		const val = inst.get();
		return (
			val.indexOf(ResourceRef.typeDelim) > 0 &&
			val.indexOf(ResourceRef.typeDelim) < val.indexOf(ResourceRef.moduleDelim)
		);
	}

	parse() {
		return this.parseType().parseModule().parsePath();
	}

	get() {
		return this.type + ResourceRef.typeDelim + this.module + ResourceRef.moduleDelim + this.path;
	}

	build(type, moduleName, bias) {
		if(!this.path) return this.parse().build.apply(this,arguments);

		if( (bias && this.type) || (this.type && !type) ) type = this.type;
		if( (bias && this.module) || (this.module && !moduleName) ) moduleName = this.module;

		return this.setType(type).setModule(moduleName);
	}

	createRelative(relRef, type) {
		let newRef = new ResourceRef(this);
		if(typeof type === "string") newRef.setType(type);
		newRef._ref = String(relRef);
		return newRef.parse();
	}

	setType(type) {
		let errorIndex;

		errorIndex = type.indexOf(ResourceRef.moduleDelim);
		if(errorIndex > -1) throw new SyntaxError(ERRORS.Syntax.ModuleDelim(type, errorIndex));

		errorIndex = type.indexOf(ResourceRef.pathSep);
		if(errorIndex > -1) throw new SyntaxError(ERRORS.Syntax.PathSep(type, errorIndex));

		return this.type = type.toLowerCase(), this;
	}

	setModule(moduleName) {
		let errorIndex;

		errorIndex = moduleName.indexOf(ResourceRef.typeDelim);
		if(errorIndex > -1) throw new SyntaxError(ERRORS.Syntax.TypeDelim(moduleName, errorIndex));

		errorIndex = moduleName.indexOf(ResourceRef.pathSep);
		if(errorIndex > -1) throw new SyntaxError(ERRORS.Syntax.PathSep(moduleName, errorIndex));

		return this.module = moduleName, this;
	}

	setPath(path) {
		path = this.normalizePath(path);

		let errorIndex;

		errorIndex = path.indexOf(ResourceRef.moduleDelim);
		if(errorIndex > -1) throw new SyntaxError(ERRORS.Syntax.ModuleDelim(path, errorIndex));

		errorIndex = path.indexOf(ResourceRef.typeDelim);
		if(errorIndex > -1) throw new SyntaxError(ERRORS.Syntax.TypeDelim(path, errorIndex));

		return this.path = path, this;
	}

	parseType() {
		let ref = this._ref;
		let type = ref.split(ResourceRef.typeDelim)[0];

		if(type === ref) return this;

		return this.setType(type);
	}

	parseModule() {
		let ref = this._ref;
		let withoutType = ref.substr(ref.indexOf(ResourceRef.typeDelim)+1);
		let modName = withoutType.split(ResourceRef.moduleDelim)[0];

		if(modName === withoutType) return this;

		return this.setModule(modName);
	}

	parsePath() {
		let ref = this._ref;
		let withoutType = ref.substr(ref.indexOf(ResourceRef.typeDelim)+1);
		let newPath = withoutType.substr(withoutType.indexOf(ResourceRef.moduleDelim)+1);

		return this.setPath(newPath);
	}

	normalizePath(path) {
		const pathSep = ResourceRef.pathSep;
		let newPath = pathSep + (path.split(
			new RegExp(pathSep+"+")
		).filter( (v) => !!v ).join(pathSep));

		let reg = new RegExp(
			pathSep+"[^"+pathSep+"]+"+pathSep+"\\\.\\\.("+pathSep+"|$)"
		)

		while(reg.test(newPath)) newPath = newPath.replace(reg, pathSep);

		newPath = newPath.replace(new RegExp(pathSep+"."+pathSep, "g"), pathSep);

		return (newPath.split(pathSep).filter( (v) => !!v ).join(pathSep));
	}

	serialize() {
		let str = this.get();
		let buffer = "";

		if(typeof btoa === "function") {
			buffer = btoa(str);
		} else {
			if (!(str instanceof Buffer)) {
				buffer = new Buffer(str.toString(), 'binary');
			}
			buffer = buffer.toString('base64');
		}

		return buffer
			.replace(/\//g,'_')
			.replace(/\+/g,'-')
			.replace(/\=/g,'')
		;
	}

	unserialize(serializedPath) {
		let str = serializedPath.replace(/\-/g,'+').replace(/\_/g,'/');

		if(typeof atob === "function") this._ref = atob(str);
		else this._ref = new Buffer(str, 'base64').toString("ascii");

		return this.parse();
	}

	toString() {
		return this.get();
	}
}

export default ResourceRef;

Object.assign(ERRORS, {
	Syntax: {
		ModuleDelim: function(r, i) {
			return ERRORS.Syntax.Generic("Module Delimiter",r,i);
		},
		PathSep: function(r, i) {
			return ERRORS.Syntax.Generic("Path Seperator",r,i);
		},
		TypeDelim: function(r, i) {
			return ERRORS.Syntax.Generic("Type Delimiter",r,i);
		},
		Generic: function(t, r, i) {
			return "Unexpected "+t+"; Column "+i+" of `"+r+"`";
		}
	}
});

Object.defineProperties(ResourceRef, {
	"typeDelim": {
		enumerable: false,
		writable: false,
		configurable: false,
		value: "$"
	},
	"moduleDelim": {
		enumerable: false,
		writable: false,
		configurable: false,
		value: ":"
	},
	"pathSep": {
		enumerable: false,
		writable: false,
		configurable: false,
		value: "/"
	}
});



/*
function ResourcePathNormalizer(NeueUI, origPath) {
	let detached = detachType(origPath);
	let type = detached[0];
	let ref = detached[1];

	let parts = ref.split(/\//).filter(v => !!v);

	let siteModName = parts.splice(0,1);
	let siteModNameLC = siteModName.toLowerCase();

	let newPath = parts.join("/");

	if(siteModNameLC === "base"
	|| siteModNameLC === "site") {
		return [ NeueUI.SiteModules.__default, newPath ]
	}

	if(NeueUI.SiteModules.hasOwnProperty(siteModName)) return [siteModName, newPath];
	else return [ NeueUI.SiteModules.__default, ref ];
}
*/
