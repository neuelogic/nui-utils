export default class SingletonPromise {
	constructor(resolver, defaultCatch) {
		this.defaultCatch = defaultCatch || this.constructor.defaultCatch;

		this.__val = void 0;
		this.__resolved = void 0;

		this.__listeners = {
			thens: [],
			catches: []
		};

		try {
			resolver.apply(this, [
				(val) => this.__resolve(val, true),
				(e) => this.__resolve(e, false)
			]);
		} catch(e) {
			this.__resolve(e, false, true);
		}
	}

	__resolve(val,success,force) {
		if(this.constructor.alwaysForce===false) {
			if(this.__resolved !== void 0 && force !== true) return;
		}
		this.__resolved = success;
		this.__val = val;

		if(success===true) {
			try {
				this.__listeners.thens.map( (l) => l(val) );
			} catch(e) {
				this.__resolve(e, false, true);
			}
		}
		else if(success===false) {
			if(this.__listeners.catches.length === 0) this.defaultCatch(val)
			else this.__listeners.catches.map( (l) => l(val) );
		} else throw new TypeError('SingletonPromise: Resolver requires `lvl` to be `true` or `false`');
	}

	then(fulfilled, rejected) {
		if(!!fulfilled) {
			this.__listeners.thens.push(fulfilled);
			if(this.__resolved===true) {
				try {
					fulfilled(this.__val);
				} catch(e) {
					this.__resolve(e, false, true);
				}
			}
		}
		if(!!rejected) {
			this.__listeners.catches.push(rejected);
			if(this.__resolved===false) {
				rejected(this.__val);
			}
		}

		return this;
	}

	catch(rejected) {
		return this.then(void 0, rejected);
	}
}

Object.defineProperty(SingletonPromise, 'defaultCatch', {
	'enumerable': false,
	'configurable': false,
	'writable': true,
	'value': () => {}
});

Object.defineProperty(SingletonPromise, 'alwaysForce', {
	'enumerable': false,
	'configurable': false,
	'writable': true,
	'value': false
});
