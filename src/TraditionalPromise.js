export default class TraditionalPromise {
	constructor(resolver, defaultCatch) {
		this.defaultCatch = defaultCatch || this.constructor.defaultCatch;

		this.__val = void 0;
		this.__resolved = void 0;

		this.__listeners = {
			thens: [],
			catches: []
		};

		try {
			resolver.apply(this, [
				(val) => this.__resolve(val, true),
				(e) => this.__resolve(e, false)
			]);
		} catch(e) {
			this.__resolve(e, false, true);
		}
	}

	__resolve(val,success,force) {
		if(this.__resolved !== void 0 && force !== true) return;
		this.__resolved = success;
		this.__val = val;

		if(success===true) {
			try {
				this.__listeners.thens.map( (l) => l(val) );
			} catch(e) {
				this.__resolve(e, false, true);
			}
		}
		else if(success===false) {
			if(this.__listeners.catches.length === 0) this.defaultCatch(val)
			else this.__listeners.catches.map( (l) => l(val) );
		} else throw new TypeError('TraditionalPromise: Resolver requires `lvl` to be `true` or `false`');
	}

	then(fulfilled, rejected) {
		var parentPromise = this;

		return new TraditionalPromise(function(res,rej) {
			if(!!fulfilled) {
				const thenFn = (val) => {
					try {
						res(fulfilled(val));
					} catch(e) {
						rej(e);
					}
				};
				parentPromise.__listeners.thens.push(thenFn);
				if(parentPromise.__resolved===true) thenFn(parentPromise.__val);
			}

			if(!!rejected) {
				const catchFn = (e) => rej(rejected(e));
				parentPromise.__listeners.catches.push(catchFn);
				if(parentPromise.__resolved===false) catchFn(parentPromise.__val);
			}
		}, parentPromise.defaultCatch);
	}

	catch(rejected) {
		return this.then(void 0, rejected);
	}
}

Object.defineProperty(TraditionalPromise, 'defaultCatch', {
	'enumerable': false,
	'configurable': false,
	'writable': true,
	'value': () => {}
});
