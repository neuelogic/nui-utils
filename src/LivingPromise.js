export default class LivingPromise {
	constructor(resolver, defaultCatch) {
		this.defaultCatch = defaultCatch || this.constructor.defaultCatch;

		this.__val = void 0;
		this.__resolved = void 0;

		this.__listeners = {
			thens: [],
			catches: []
		};

		try {
			resolver.apply(this, [
				(val) => this.__resolve(val, true),
				(e) => this.__resolve(e, false),
				() => this.__val = this.__resolved = void 0
			]);
		} catch(e) {
			this.__resolve(e, -1);
		}
	}

	__resolve(val,success) {
		this.__resolved = success;
		this.__val = val;

		if(success===true) this.__listeners.thens.map( (l) => l(val) );
		else if(success===false)  {
			if(this.__listeners.catches.length === 0) this.defaultCatch(val)
			else this.__listeners.catches.map( (l) => l(val) );
		} else throw new TypeError('LivingPromise: Resolver requires `lvl` to be `true` or `false`');
	}

	then(fulfilled, rejected) {
		var parentPromise = this;

		return new LivingPromise(function(res,rej) {
			if(!!fulfilled) {
				const thenFn = (val) => {
					try {
						res(fulfilled(val));
					} catch(e) {
						rej(e);
					}
				};
				parentPromise.__listeners.thens.push(thenFn);
				if(parentPromise.__resolved===true) thenFn(parentPromise.__val);
			}

			if(!!rejected) {
				const catchFn = (e) => rej(rejected(e));
				parentPromise.__listeners.catches.push(catchFn);
				if(parentPromise.__resolved===false) catchFn(parentPromise.__val);
			}
		}, parentPromise.defaultCatch);
	}

	catch(rejected) {
		return this.then(void 0, rejected);
	}
}

Object.defineProperty(LivingPromise, 'defaultCatch', {
	'enumerable': false,
	'configurable': false,
	'writable': true,
	'value': () => {}
});
