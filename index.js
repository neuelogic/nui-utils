const TraditionalPromise = require('./lib/TraditionalPromise.js').default;

module.exports = {
	AjaxUtility: require('./lib/AjaxUtility.js').default,
	DeepAssign: require('./lib/DeepAssign.js').default,
	Dispatcher: require('./lib/Dispatcher.js').default,
	Interface: require('./lib/Interface.js').default,
	LivingPromise: require('./lib/LivingPromise.js').default,
	Promise: TraditionalPromise,
	ResourceRef: require('./lib/ResourceRef.js').default,
	Serializable: require('./lib/Serializable.js').default,
	SingletonPromise: require('./lib/SingletonPromise.js').default,
	TraditionalPromise: TraditionalPromise,
	ValueTypeFilters: require('./lib/ValueTypeFilters.js').default
};
